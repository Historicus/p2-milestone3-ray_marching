/**********************************************
* Author: Samuel Kenney 2/8/2017, modified 2/17/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Voxel Buffer class
*
**********************************************/

#pragma once

#include "VoxelBuffer.h"

//constructor for Voxel Buffer class
VoxelBuffer::VoxelBuffer(float delta, const ivec3& dimensions,int h, int w, int d){
	voxelBuffer = NULL;
	this->delta = delta;
	xyzCoords = dimensions;
	height = h;
	width = w;
	depth = d;
}

//destructor for Voxel Buffer class
VoxelBuffer::~VoxelBuffer(void){
	delete [] voxelBuffer;
}

//grabs density value when given coordinates
float VoxelBuffer::densityRead(const gVector4& coords) const{
	//px > delta*xsize
	//py > delta*ysize
	//pz > delta*zsize;

	//check if entered the buffer yet
	if ((coords[0] >= xyzCoords.x*delta) || (coords[1] >= xyzCoords.y*delta) || (coords[2] >= xyzCoords.z*delta)
		|| (coords[0] < 0.0f)||(coords[1] < 0.0f)||(coords[2] < 0.0f)){
		return 0.0f;
	}

	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	//if out of bounds access
	//BAD!!
	//if (index < 0){
	//	return 0.0;
	//}

	return voxelBuffer[index].first;
}

//grabs light value when given coordinates
float VoxelBuffer::lightRead(const gVector4& coords) const{
	//if entered the buffer
	if ((coords[0] >= xyzCoords.x*delta) || (coords[1] >= xyzCoords.y*delta) || (coords[2] >= xyzCoords.z*delta)||
		(coords[0] < 0.0f)||(coords[1] < 0.0f)||(coords[2] < 0.0f)){
		return 0.0f;
	}
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	//bad!!!
	//if out of bounds access
	//if (index < 0){
	//	return 0.0;
	//}

	return voxelBuffer[index].second;
}

//writes new density value when given coordinates
void VoxelBuffer::densityWrite(const gVector4& coords, float value){
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;


	voxelBuffer[index].first = value;
}

//writes new light value when given coordinates
void VoxelBuffer::lightWrite(const gVector4& coords, float value){
	int i, j, k, index;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	//index = k * width    *  height    +   j * width      + i;
	index = (k * xyzCoords.x * xyzCoords.y)  + (j * xyzCoords.x) + i;

	voxelBuffer[index].second = value;
}

//returns coordinates of center of the voxel specified with coordinates
gVector4 VoxelBuffer::getVoxelCenter(const gVector4& coords) const{

	//get indices from coordniates and then add half of delta to get to the center of the voxel
	int i, j, k;
	i = coords[0]/this->delta;
	j = coords[1]/this->delta;
	k = coords[2]/this->delta;
	
	float xCoord = i*this->delta + this->delta/2;
	float yCoord = j*this->delta + this->delta/2;
	float zCoord = k*this->delta + this->delta/2;
	gVector4 coordinates(xCoord, yCoord, zCoord, 1.0f);
	return coordinates;
}

//returns coordinates of center of the voxel specified with indices
gVector4 VoxelBuffer::getVoxelCenter(const ivec3& coords) const{

	//get indices and then add half of delta to get to the center of the voxel
	float xCoord = coords.x*this->delta + this->delta/2;
	float yCoord = coords.y*this->delta + this->delta/2;
	float zCoord = coords.z*this->delta + this->delta/2;

	gVector4 coordinates(xCoord, yCoord, zCoord, 1.0f);
	return coordinates;
}