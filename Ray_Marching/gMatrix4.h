//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania
 
//modified January 25, 2017 at Grove City College
 
#pragma once
 
#include "gVector4.h"

class gMatrix4{
private:
	gVector4 data[4];
public:
	///----------------------------------------------------------------------
	/// Constructors
	///----------------------------------------------------------------------
	/// Default Constructor.  Initialize to matrix of all 0s.
	gMatrix4();

	/// Initializes matrix with each vector representing a row in the matrix
	gMatrix4(const gVector4& row0, const gVector4& row1, const gVector4& row2, const gVector4& row3);

	///----------------------------------------------------------------------
	/// Getters
	///----------------------------------------------------------------------	
	/// Returns the values of the row at the index
	gVector4 operator[](unsigned int index) const;
	
	/// Returns a reference to the row at the index
	gVector4& operator[](unsigned int index);
	
	/// Returns a column of the matrix
	gVector4 getColumn(unsigned int index) const;

	/// Prints the matrix to standard output in a nice format
	void print();

	///----------------------------------------------------------------------
	/// Matrix Operations
	///----------------------------------------------------------------------	
	/// Returns the transpose of the matrix (v_ij == v_ji)
	gMatrix4 transpose() const;

	///----------------------------------------------------------------------
	/// Static Initializers
	///----------------------------------------------------------------------	
	/// Creates a 3-D rotation matrix.
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the x-axis
	static gMatrix4 rotationX(float angle);
	
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the y-axis
	static gMatrix4 rotationY(float angle);
	
	/// Takes an angle in degrees and outputs a 4x4 rotation matrix for rotating around the z-axis
	static gMatrix4 rotationZ(float angle);
	
	/// Takes an x, y, and z displacement and outputs a 4x4 translation matrix
	static gMatrix4 translation2D(float x, float y, float z);
	
	/// Takes an x, y, and z scale and outputs a 4x4 scale matrix
	static gMatrix4 scale2D(float x, float y, float z);
	
	/// Generates a 4x4 identity matrix
	static gMatrix4 identity();	
	
	
	///----------------------------------------------------------------------
	/// Friend Functions
	///----------------------------------------------------------------------
	/// Checks if m1 == m2
 	bool operator==(const gMatrix4& m2);
	
	/// Checks if m1 != m2
	bool operator!=(const gMatrix4& m2);
	
	/// Matrix addition (m1 + m2)
	gMatrix4 operator+(const gMatrix4& m2);
	
	/// Matrix subtraction (m1 - m2)
	gMatrix4 operator-(const gMatrix4& m2);
	
	/// Matrix multiplication (m1 * m2)
	gMatrix4 operator*(const gMatrix4& m2);
	
	/// Matrix/vector multiplication (m * v)
	/// Assume v is a column vector (ie. a 4x1 matrix)
	gVector4 operator*(const gVector4& v);
};
