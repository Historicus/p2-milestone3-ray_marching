/**********************************************
* Author: Samuel Kenney 2/24/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Ray Marching class
*
**********************************************/
#include "ray_marching.h"

//constructor
ray_marching::ray_marching(float STEP, gVector4 BRGB, gVector4 MRGB, gVector4 LPOS, gVector4 LCOL, std::string OUTPUT,VoxelBuffer* vox, ray_generator *raygen)
{
	step = STEP;
	brgb = BRGB;
	mrgb = MRGB;
	lpos = LPOS;
	lcol = LCOL;
	outputFile = OUTPUT;
	T = 1.0f;
	Xe = raygen->eyePos;
	buffer = vox;
	rays = raygen;
}

//destructor
ray_marching::~ray_marching(void)
{
	delete buffer;
	delete rays;
}

//generating the color
gVector4 ray_marching::color(int x, int y){
	gVector4 color;

	//Xi starts at Xe
	gVector4 Xi = Xe;
	//start T at 1
	T = 1;
	float density = 0.0f;
	//generate ray
	gVector4 ray = rays->generateRay(x, y);

	//if convergence, checking for outside takes place in densityRead and further down while loop
	while(T > 0.00001f){
		//Xi += n * stepsize
		Xi = Xi + ray * step;

		//check if exited the buffer, when z is less than 0
		if (Xi[2] < 0.0f){
			break;
		}

		//grab density value
		density = buffer->densityRead(Xi);

		//change of T
		float changeT = exp(-1 * step * density);

		//T *= change of T
		T *= changeT;

		//check if light value is -1, if so, calculate light value
		float lightValue = buffer->lightRead(Xi);
		if (lightValue == -1.0f){
			lightValue = light(ray, Xi);
		}

		//(1 - change of T / K) (materialColor * lightColor) * T * getVoxelLight (if -1, calculate);
		color = color +  (( 1 - changeT) / 1) * (mrgb ^ lcol) * T * lightValue;
	}
	//add background color
	gVector4 t = T * brgb;
	color = color + t;

	return color;
}

//genereting light value
float ray_marching::light(gVector4 n, gVector4 X){
	float lightValue;
	//get Xn value from Xi
	gVector4 Xn(X[0], X[1], X[2], 0.0f);

	//start T2 at 1
	float T2 = 1.0f;

	//split components up
	float first = (lpos*lpos);
	float second = (X*X);
	float last =  first + second;
	//length of vector
	float D = std::sqrt(last);

	//calculate N, unit vector in direction of lpos
	gVector4 N = ((lpos - X)/D);
	//figure out how many steps to take
	int num = D / step;
	//counter for know how many steps took
	int count = 0;

	//if no convergence and not at light source, calculate light
	while ((T2 > 0.000001f) && (count < num)){
		//step along ray
		Xn = Xn + (N * step);
		//add density value
		float changeT2 = exp(-1 * step * buffer->densityRead(Xn));
		T2 *= changeT2;

		count++;
	}
	lightValue = T2;
	//write light value to keep from recalculating later
	buffer->lightWrite(X, lightValue);

	return lightValue;
}


//grab information from file, dynamically instantiate other classes here as well
ray_marching* ray_marching::factory(std::string filename){
	//dynamically instantiate in factory
	VoxelBuffer *newBuffer;
	

	//Start to parse file and fill necessary values to then instantiate classes

	//only need one file stream
	std::ifstream ray_march_text (filename);
	//only need one string for value
	std::string value;
	//only need one string for multiple parsed values
	std::string numInTxt;

	//*************************VOXEL BUFFER**********************************
	//will hold delta value
	float delt = 0.0;
	//will be used to store XYZC values
	ivec3 sizeCoords;
	//*************************************************************************

	//*************************RAY GENERATION**********************************
	//get filename
	std::string fileBMP = "";
	//reso
	int width = 0, height = 0;
	//eyep
	gVector4 eyep(0.0,0.0,0.0,0.0);
	//vdir
	gVector4 vdir(0.0,0.0,0.0,0.0);
	//uvec
	gVector4 uvec(0.0,0.0,0.0,0.0);
	//fovy
	float fieldofV = 0.0f;
	//*************************************************************************

	//*************************RAY GENERATION**********************************
	float STEP;
	//background color
	gVector4 BRGB(0.0,0.0,0.0,0.0);
	//material color
	gVector4 MRGB(0.0,0.0,0.0,0.0);
	//light position
	gVector4 LPOS(0.0,0.0,0.0,0.0);
	//light color
	gVector4 LCOL(0.0,0.0,0.0,0.0);
	//*************************************************************************


	//FORMAT FOR TEXT FILE
	//DELT 1
	//STEP 0.5
	//XYZC 100 100 100
	//BRGB 0.27 0.51 0.71
	//MRGB 0.96 0.96 0.96
	//FILE BabysFirstCloudImage.bmp
	//RESO 640 480
	//EYEP 50 50 200
	//VDIR 0 0 -1
	//UVEC 0 1 0
	//FOVY 45
	//LPOS 200 0 0
	//LCOL 1 1 1
	//float ... float


	//Index for place in text file
	int in = 0;
	//Index when creating VoxelBuffer
	int voxelIn = 0;
	//size of buffer
	int size = 0;
	while (getline(ray_march_text, value)){
		//remove beginning of text if not in density values
		if (in < 13){ value.erase(0,5);}
		if (in == 0){
			//if DELT value convert to float
			delt = std::stof(value);
			std::cout << "DELT " << delt << std::endl;
		} else if (in == 1){
			//grab STEP value
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store STEP value
			STEP = std::stof(numInTxt);
			std::cout << "STEP " << STEP << std::endl;
		} else if (in == 2){
			//grab X
			numInTxt = value.substr(0, value.find(" "));
			//remove X from string
			value.erase(0, value.find(" ")+1);
			//store X
			sizeCoords.x = std::stoi(numInTxt);

			//grab Y
			numInTxt = value.substr(0, value.find(" "));
			//remove Y from string
			value.erase(0, value.find(" ")+1);
			//store Y
			sizeCoords.y = std::stoi(numInTxt);

			//grab Z
			numInTxt = value.substr(0, value.find(" "));
			//remove Z from string
			value.erase(0, value.find(" ")+1);
			//store Z
			sizeCoords.z = std::stoi(numInTxt);
			std::cout << "XYZC " << sizeCoords.x << ", " << sizeCoords.y << ", " << sizeCoords.z << std::endl;
		} else if (in == 3){
			//grab R
			numInTxt = value.substr(0, value.find(" "));
			//remove R from string
			value.erase(0, value.find(" ")+1);
			//store R
			BRGB[0] = std::stof(numInTxt);

			//grab G
			numInTxt = value.substr(0, value.find(" "));
			//remove G from string
			value.erase(0, value.find(" ")+1);
			//store G
			BRGB[1] = std::stof(numInTxt);

			//grab B
			numInTxt = value.substr(0, value.find(" "));
			//remove B from string
			value.erase(0, value.find(" ")+1);
			//store B
			BRGB[2] = std::stof(numInTxt);
			std::cout << "BRGB " << BRGB[0] << ", " << BRGB[1] << ", " << BRGB[2] << std::endl;
		} else if (in == 4){
			//grab R
			numInTxt = value.substr(0, value.find(" "));
			//remove R from string
			value.erase(0, value.find(" ")+1);
			//store R
			MRGB[0] = std::stof(numInTxt);

			//grab G
			numInTxt = value.substr(0, value.find(" "));
			//remove G from string
			value.erase(0, value.find(" ")+1);
			//store G
			MRGB[1] = std::stof(numInTxt);

			//grab B
			numInTxt = value.substr(0, value.find(" "));
			//remove B from string
			value.erase(0, value.find(" ")+1);
			//store B
			MRGB[2] = std::stof(numInTxt);
			std::cout << "MRGB " << MRGB[0] << ", " << MRGB[1] << ", " << MRGB[2] << std::endl;
		} else if (in == 5){
			//grab FILE value
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store FILE value
			fileBMP = numInTxt;
			std::cout << "FILE " << fileBMP << std::endl;
		} else if ( in == 6) {
			//grab first value of res
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store first value
			width = std::stoi(numInTxt);

			//grab second value of res
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store second value
			height = std::stoi(numInTxt);

			std::cout << "RES  " << width << " " << height << std::endl;

		} else if ( in == 7){
			//grab x value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			eyep[0] = std::stof(numInTxt);

			//grab y value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			eyep[1] = std::stof(numInTxt);

			//grab z value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			eyep[2] = std::stof(numInTxt);

			std::cout << "EYEP " << eyep[0] << " " << eyep[1] << " " << eyep[2] << std::endl;

		} else if ( in == 8){
			//grab x value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			vdir[0] = std::stof(numInTxt);

			//grab y value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			vdir[1] = std::stof(numInTxt);

			//grab z value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			vdir[2] = std::stof(numInTxt);

			std::cout << "VDIR " << vdir[0] << " " << vdir[1] << " " << vdir[2] << std::endl;
		} else if ( in == 9){
			//grab x value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			uvec[0] = std::stof(numInTxt);

			//grab y value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			uvec[1] = std::stof(numInTxt);

			//grab z value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			uvec[2] = std::stof(numInTxt);

			std::cout << "UVEC " << uvec[0] << " " << uvec[1] << " " << uvec[2] << std::endl;
		} else if ( in == 10){
			//grab FOVY value
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store FOVY value
			fieldofV = std::stof(numInTxt);
			std::cout << "FOVY " << fieldofV << std::endl;
		} else if (in == 11){
			//grab LPOS X
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS X from string
			value.erase(0, value.find(" ")+1);
			//store LPOS X
			LPOS[0] = std::stof(numInTxt);

			//grab LPOS Y
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS Y from string
			value.erase(0, value.find(" ")+1);
			//store LPOS Y
			LPOS[1] = std::stof(numInTxt);

			//grab LPOS Z
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS Z from string
			value.erase(0, value.find(" ")+1);
			//store LPOS Z
			LPOS[2] = std::stof(numInTxt);
			std::cout << "LPOS " << LPOS[0] << ", " << LPOS[1] << ", " << LPOS[2] << std::endl;
		} else if (in == 12){
			//grab LCOL X
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS X from string
			value.erase(0, value.find(" ")+1);
			//store LPOS X
			LCOL[0] = std::stof(numInTxt);

			//grab LPOS Y
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS Y from string
			value.erase(0, value.find(" ")+1);
			//store LPOS Y
			LCOL[1] = std::stof(numInTxt);

			//grab LPOS Z
			numInTxt = value.substr(0, value.find(" "));
			//remove LPOS Z from string
			value.erase(0, value.find(" ")+1);
			//store LPOS Z
			LCOL[2] = std::stof(numInTxt);
			std::cout << "LCOL " << LCOL[0] << ", " << LCOL[1] << ", " << LCOL[2] << std::endl;
		} else{
			
			//if first density value
			if (in == 13){
				//instantiate Voxel Buffer
				//create new VoxelBuffer with previous read in delta value and XYZ coordinates
				
				newBuffer = new VoxelBuffer(delt, sizeCoords, sizeCoords.x, sizeCoords.y, sizeCoords.z);
				//dynamically allocate space for array to hold density value and light value
				int size = sizeCoords.x * sizeCoords.y * sizeCoords.z;
				newBuffer->voxelBuffer = new std::pair<float, float>[size];
			}
			//read in density and light values
			newBuffer->voxelBuffer[voxelIn] = std::make_pair(std::stof(value), -1.0f);
			voxelIn++;
		}

		in++;
	}
	ray_generator *rays;
	rays = new ray_generator(fileBMP, width, height, eyep, vdir, uvec, fieldofV);
	//generate vectors
	rays->FirstBasisVector();
	rays->SecondBasisVector();
	rays->ThirdBasisVector();
	rays->calculateMVH();

	//initialize Ray march
	ray_marching *rayMarch;
	rayMarch = new ray_marching(STEP, BRGB, MRGB, LPOS, LCOL, fileBMP, newBuffer, rays);

	ray_march_text.close();

	return rayMarch;
}
