/**********************************************
* Author: Samuel Kenney 2/24/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Ray Marching header
*
**********************************************/

#include "gMatrix4.h"
#include "gVector4.h"
#include "VoxelBuffer.h"
#include "ray_generator.h"
#include "ivec3.h"
#include <string>
#include <fstream>
#include <cmath>

#pragma once

class ray_marching
{
private:
	//step size used for moving along ray
	float step;
	//background color
	gVector4 brgb;
	//material color
	gVector4 mrgb;
	//light position
	gVector4 lpos;
	//light color
	gVector4 lcol;
	//transmittance
	float T;
	//Xe
	gVector4 Xe;
	
public:
	//string to hold BMP file
	std::string outputFile;
	//two classes needed
	VoxelBuffer *buffer;
	ray_generator *rays;

	//constructor
	ray_marching(float, gVector4, gVector4, gVector4, gVector4, std::string, VoxelBuffer*, ray_generator *);
	//destructor
	~ray_marching(void);

	//generating the color
	gVector4 color(int , int);

	//genereting light value
	float light(gVector4, gVector4);

	//grab information from file, dynamically instantiate other classes here as well
	static ray_marching* factory(std::string);
};

